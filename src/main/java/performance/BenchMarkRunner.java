package performance;

import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class BenchMarkRunner {

    public static void main(String[] args) throws Exception {

        Options opt = new OptionsBuilder()
                .include(BenchMark_DW.class.getSimpleName())
                .build();
        new Runner(opt).run();
    }

}
