package performance;

import org.mule.runtime.api.el.BindingContext;
import org.mule.runtime.api.metadata.DataType;
import org.mule.runtime.api.metadata.MediaType;
import org.mule.runtime.api.metadata.TypedValue;
import org.mule.runtime.core.internal.el.DefaultBindingContextBuilder;
import org.mule.runtime.core.internal.util.rx.ImmediateScheduler;
import org.mule.weave.v2.el.MuleServiceLevelModuleManager;
import org.mule.weave.v2.el.WeaveExpressionLanguage;
import org.mule.weave.v2.model.service.DefaultCharsetProviderService$;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.stream.Collectors;

public class DataWeave {

    private final WeaveExpressionLanguage lang = new WeaveExpressionLanguage(ImmediateScheduler.IMMEDIATE_SCHEDULER,
            DefaultCharsetProviderService$.MODULE$,
            new MuleServiceLevelModuleManager());

    private final DataType jsonString = DataType.builder()
            .type(String.class)
            .mediaType(MediaType.APPLICATION_JSON)
            .build();

    private BindingContext payload = new DefaultBindingContextBuilder()
            .addBinding("payload", new TypedValue<>("{}", jsonString)).build();

    public DataWeave(){}

    public String execute(String script){
        TypedValue<?> evaluate = lang.evaluate(script, DataType.JSON_STRING, payload);
        return evaluate.getValue().toString();
    }

    public void setPayload(String payloadFile) throws IOException {
        String payloadValue = getFileFromResources(payloadFile);
        this.payload = new DefaultBindingContextBuilder()
                .addBinding("payload", new TypedValue<>(payloadValue, jsonString)).build();
    }


    public String getFileFromResources(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        Objects.requireNonNull(this.getClass().getClassLoader().getResourceAsStream(fileName))));
        return reader.lines().collect(Collectors.joining("\n"));
    }
}
