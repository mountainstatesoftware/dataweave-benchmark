package performance;

import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Warmup(iterations = 10, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, timeUnit = TimeUnit.MILLISECONDS)
@Fork(value = 1)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.AverageTime)
public class BenchMark_DW {


    @State(Scope.Benchmark)
    public static class ExecutionPlan {
        
        @Param({ "10" })
        public int iterations;

        DataWeave dw;
        String header = "%dw 2.0\n" +
                "output application/json\n";

        @Setup(Level.Trial)
        public void setUp() {
            dw = new DataWeave();
        }
    }



    @Benchmark
    public void abs(ExecutionPlan plan) {
        String script = plan.header +
                "---\n " +
                "abs(-2)";
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);
        }
    }

    @Benchmark
    public void avg(ExecutionPlan plan) {
        String script = plan.header +
                "---\n " +
                "avg([1,2,3,4,5,6,7,8,9,10])";
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }


    @Benchmark
    public void ceil(ExecutionPlan plan) {
        String script = plan.header +
                "---\n " +
                "ceil(2.1)";
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void contains(ExecutionPlan plan) {
        String script = plan.header +
                "---\n " +
                "[ 1, 2, 3, 4 ] contains(2)";
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void daysBetween(ExecutionPlan plan) {
        String script = plan.header +
                "---\n " +
                "daysBetween('2016-10-01T23:57:59-03:00', '2017-10-01T23:57:59-03:00')";
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void distinctBy(ExecutionPlan plan) {
        String script = plan.header +
                "---\n " +
                "[0,1,2,3,3,2,1,4] distinctBy (value) -> { \"unique\" : value }";
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void endsWith(ExecutionPlan plan) {
        String script = plan.header +
                "---\n " +
                "\"Hello World\" endsWith  \"World\"";
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void entriesOf(ExecutionPlan plan) {
        String script = plan.header +
                "---\n " +
                "entriesOf({a:1})";
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void filter(ExecutionPlan plan) {
        String script = plan.header +
                "---\n " +
                "[9,2,3,4,5] filter (value, index) -> (value > 2)";
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void filterObject(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n " +
                        "{\"a\" : \"apple\", \"b\" : \"banana\"} filterObject ((value) -> value == \"apple\")");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void find(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n " +
                        "[\"Bond\", \"James\", \"Bond\"] find \"Bond\"");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void flatMap(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "[ [3,5], [0.9,5.5] ] flatMap (value, index) -> value\n");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void flatten(ExecutionPlan plan) {
        String script = plan.header + (
                "var array1 = [1,2,3]\n" +
                        "var array2 = [4,5,6]\n" +
                        "var array3 = [7,8,9]\n" +
                        "var arrayOfArrays = [array1, array2, array3]\n" +
                        "---\n" +
                        "flatten(arrayOfArrays)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void floor(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "floor(1.9)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void groupBy(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "[\"a\",\"b\",\"c\"] groupBy (item, index) -> index");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void isBlank(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "isBlank(\"\")");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void isDecimal(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "isDecimal(1.2)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void isEmpty(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "isEmpty({})");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void isEven(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "isEven(2)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void isInteger(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "isInteger(2)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void isLeapYear(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "isLeapYear(|2016-10-01T23:57:59|)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void isOdd(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "isOdd(1)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void joinBy(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "[\"a\",\"b\",\"c\"] joinBy \"-\"");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void keysOf(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "keysOf({ \"a\" : true, \"b\" : 1})");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void lower(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "lower(\"HELLO\")");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void map(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "[\"jose\", \"pedro\", \"mateo\"] map (value, index) -> { (index) : value}\n");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void mapObject(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "{\"a\":\"b\",\"c\":\"d\"} mapObject (value,key,index) -> { (index) : { (value):key} }\n");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void match(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "\"test@server.com\" match(/([a-z]*)@([a-z]*).com/)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void matches(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "[ (\"admin123\" matches /a.*\\d+/), (\"admin123\" matches /^b.+/) ]\n");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void max(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "max([1, 1000])");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void maxBy(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "[ { \"a\" : 1 }, { \"a\" : 3 }, { \"a\" : 2 } ] maxBy ((item) -> item.a)\n");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void min(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "min([1, 1000])");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void minBy(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "[ { \"a\" : 1 }, { \"a\" : 3 }, { \"a\" : 2 } ] minBy ((item) -> item.a)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void mod(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "(3 mod 2)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void namesOf(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        " namesOf({ \"a\" : true, \"b\" : 1})");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void orderBy(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "[{ letter: \"e\" }, { letter: \"d\" }] orderBy($.letter)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void pluck(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "{\"a\":\"b\",\"c\":\"d\"} pluck ((value, key, index) -> {\"value\":{(value):key}})");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);

        }
    }

    @Benchmark
    public void pow(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "(2 pow 3)");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);
        }
    }

    @Benchmark
    public void read(ExecutionPlan plan) {
        String script = plan.header + (
                "---\n" +
                        "read('{ \"hello\" : \"world\" }','application/json')");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);
        }
    }

    @Benchmark
    public void largeTest(ExecutionPlan plan) throws IOException {
        String script = plan.dw.getFileFromResources("raml2postman.dwl");
        plan.dw.setPayload("postman-input.json");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);
        }
    }

    @Benchmark
    public void mediumTest(ExecutionPlan plan) throws IOException {
        String script = plan.dw.getFileFromResources("getEmployees.dwl");
        plan.dw.setPayload("getEmployees.json");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);
        }
    }

    @Benchmark
    public void smallTest(ExecutionPlan plan) throws IOException {
        String script = plan.dw.getFileFromResources("pricing.dwl");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);
        }
    }

    @Benchmark
    public void mediumTestTwo(ExecutionPlan plan) throws IOException {
        String script = plan.dw.getFileFromResources("getEmployees-proc.dwl");
        plan.dw.setPayload("getEmployees-proc.json");
        for (int i = plan.iterations; i > 0; i--) {
            plan.dw.execute(script);
        }
    }

}
