%dw 2.0
output application/json

fun getExampleType(item) = if (item == "integer" or item == "number") 0 
                            else if (item == "string") "String"
                            else if (item == "nil" or item == "null") null
                            else if (item == "boolean") false
                            else ""

fun getTypes(types) = 
    types map ((singleType, index) -> {
        'type': singleType.name,
        example: 
        if(types[0].properties != null)
        ((
            types[0].properties) reduce ((v, obj={}) -> 
                obj ++ {(v.name): getExampleType(v.'type'[0])}
        ))
        else {}
    })

var types = getTypes(payload.specification.types)


fun resourceMap(test)=
flatten (test map ((parent, parentindex) -> {
    name: parent.relativeUri,
    description: "",
    item: if(parent.methods != null)
    (
        (parent.methods map ((methodsV, methodsI) -> 
            getRequest(methodsV, methodsI, parent) 
        )) ++
        if (parent.resources != null) 
            (resourceMap(parent.resources) map ((val) -> val )) 
        else []
    )
    else [] ++
    (
        if (parent.resources != null) 
            (resourceMap(parent.resources) map ((val) -> val )) 
        else []
    )
}))


fun getRequest(methodsV, index, parent) =
{
    name: methodsV.parentUri,
    request:{
        url:{
            raw: methodsV.absoluteParentUri
                    replace "{" with ("{{")
                    replace "}" with ("}}"),
            host: [
                methodsV.absoluteParentUri 
                    replace /https?:\/\// with("") 
                    replace /\/.+/ with ("") 
            ],
            path: [
                methodsV.absoluteParentUri 
                    replace /https?:\/\// with("") 
                    replace /^([^\/]+)/ with ("")
                    //substringAfter("/")
                    replace "{" with ("{{")
                    replace "}" with ("}}")
            ],
            query: 
            if(methodsV.queryParameters != null)
                methodsV.queryParameters map ((val, index) -> {
                    key: val.name, 
                    value: "{{" ++ val.name ++"}}",
                    equals: "{{" ++ val.name ++"}}",
                    description: ""
                })
            else [],
            variable: []
        },
        method: upper(methodsV.method),
        header:  
        if(methodsV.headers != null)
        [
            methodsV.headers map ((val, index) ->{
                key: val.name, 
                value: "{{" ++ val.name ++"}}",
                equals: "{{" ++ val.name ++"}}",
                'type': "text"
            }) 
        ]
        else [],
        body: { 
            mode: "raw",
            raw: 
                if(methodsV.body.simplifiedExamples != null)
                    methodsV.body.simplifiedExamples
                else if (methodsV.body.'type' != null)
                    write((types filter 
                        ($.'type' == methodsV.body[0].'type'[0]))[0].example) 
                else ""

        },
        description: ""
    },
    response: []
}

---
{
    variables: [],
    info:{
        name: payload.specification.title,
        '_postman_id': "8618aef7-40f6-6d2b-c397-a30998175ebb",
        description: if(payload.specification.description != null)payload.specification.description else "",
        schema: "https://schema.getpostman.com/json/collection/v2.0.0/collection.json"
    },
    item: resourceMap(payload.specification.resources)
}
