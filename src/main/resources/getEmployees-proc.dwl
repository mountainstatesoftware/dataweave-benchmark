%dw 2.0
import firstWith from dw::core::Arrays
output application/json

var persons=[
  {
    "personId": 1,
    "firstName": "Jake",
    "middleName": "Ophelia",
    "lastName": "Smith",
    "nickName": "sos",
    "dob": "1993-11-22T00:00:00",
    "addresses": [
      {
        "addressId": 3,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "Patched Address Street",
        "line2": "",
        "city": "Patched City",
        "region": "Patched Region",
        "country": "Patched country",
        "postalCode": "Patched postal code",
        "timezone": "Patched timezone",
        "tracking": {
          "createdOn": "2020-04-01 12:28:06",
          "createdBy": {
            "name": "Jane Doe",
            "id": 2
          },
          "updatedOn": "2020-07-14 06:41:08",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 2
          }
        }
      },
      {
        "addressId": 1740,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "UTC-5",
        "tracking": {
          "createdOn": "2020-07-15 16:27:06",
          "createdBy": {
            "name": "Jane Doe",
            "id": 2
          },
          "updatedOn": "2020-08-21 14:25:21",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 283,
        "title": "1",
        "note": "1",
        "tracking": {
          "createdOn": "2020-05-27 12:51:15",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-17 15:24:18",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      },
      {
        "noteId": 737,
        "title": "Flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-09-17 15:34:07",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-09-17 15:36:04",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      },
      {
        "noteId": 738,
        "title": "Flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-09-17 15:34:09",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-09-17 15:34:56",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      }
    ],
    "phones": [],
    "emails": [
      {
        "emailAddressId": 1627,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "ssmith@cfa.com",
        "tracking": {
          "createdOn": "2020-07-23",
          "createdBy": {
            "name": "Jake McJake",
            "id": 5
          },
          "updatedOn": "2020-09-16 18:05:52",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-04-01T10:50:47",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-08-21T14:25:21",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 2,
    "firstName": "Jane",
    "middleName": null,
    "lastName": "Doe",
    "nickName": "Capitan",
    "dob": "1988-07-13T00:00:00",
    "addresses": [
      {
        "addressId": 2131,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "UTC-5",
        "tracking": {
          "createdOn": "2020-09-18 03:48:03",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-09-18 04:04:00",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      },
      {
        "addressId": 2132,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "UTC-5",
        "tracking": {
          "createdOn": "2020-09-18 04:04:19",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-09-18 08:19:39",
          "updatedBy": {
            "name": null,
            "id": null
          }
        }
      },
      {
        "addressId": 2133,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "UTC-5",
        "tracking": {
          "createdOn": "2020-09-18 08:21:02",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-09-18 08:21:37",
          "updatedBy": {
            "name": null,
            "id": null
          }
        }
      },
      {
        "addressId": 2134,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "UTC-5",
        "tracking": {
          "createdOn": "2020-09-18 08:24:51",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-09-18 08:34:48",
          "updatedBy": {
            "name": null,
            "id": null
          }
        }
      },
      {
        "addressId": 2135,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "UTC-5",
        "tracking": {
          "createdOn": "2020-09-18 08:35:09",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-09-18 08:38:32",
          "updatedBy": {
            "name": null,
            "id": null
          }
        }
      },
      {
        "addressId": 2136,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "UTC-5",
        "tracking": {
          "createdOn": "2020-09-18 08:39:39",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-09-18 08:46:11",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      },
      {
        "addressId": 2137,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "UTC-5",
        "tracking": {
          "createdOn": "2020-09-18 08:50:40",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-09-18 08:50:40",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 2,
        "title": null,
        "note": "Live long and prosper",
        "tracking": {
          "createdOn": "2020-04-01 11:53:03",
          "createdBy": {
            "name": "Jane Doe",
            "id": 2
          },
          "updatedOn": "2020-06-17 09:00:27",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 2
          }
        }
      },
      {
        "noteId": 497,
        "title": "red rose",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-07-29 20:15:43",
          "createdBy": {
            "name": "Jane Doe",
            "id": 2
          },
          "updatedOn": "2020-09-18 09:08:05",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      },
      {
        "noteId": 739,
        "title": "Flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-09-18 01:43:59",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-09-18 01:43:59",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      }
    ],
    "phones": [
      {
        "phoneNumberId": 1523,
        "type": {
          "id": 2,
          "name": "work"
        },
        "countryCode": "011",
        "areaCode": "812",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-07-28 21:04:11",
          "createdBy": {
            "name": "Jane Doe",
            "id": 2
          },
          "updatedOn": "2020-07-30 14:47:59",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 2
          }
        }
      },
      {
        "phoneNumberId": 1524,
        "type": {
          "id": 2,
          "name": "work"
        },
        "countryCode": "011",
        "areaCode": "812",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-07-28 21:05:40",
          "createdBy": {
            "name": "Jane Doe",
            "id": 2
          },
          "updatedOn": "2020-07-30 14:52:54",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 2
          }
        }
      },
      {
        "phoneNumberId": 1650,
        "type": {
          "id": 1,
          "name": "home"
        },
        "countryCode": "0112",
        "areaCode": "607",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-08-19 20:02:28",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-18 09:12:26",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1825,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "ssmith@cfa.com",
        "tracking": {
          "createdOn": "2020-08-24",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-18 09:01:58",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2019-05-01T10:52:37",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-09-21T12:35:09",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 3,
    "firstName": "Beverly",
    "middleName": null,
    "lastName": "Crusher",
    "nickName": "Doctor",
    "dob": "1996-10-13T00:00:00",
    "addresses": [],
    "notes": [],
    "phones": [],
    "emails": [],
    "tracking": {
      "createdOn": "2020-04-01T10:54:12",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-08-19T21:09:07",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 4,
    "firstName": "Jake",
    "middleName": null,
    "lastName": "McJake",
    "nickName": "Clorox",
    "dob": null,
    "addresses": [],
    "notes": [],
    "phones": [],
    "emails": [
      {
        "emailAddressId": 2,
        "type": {
          "id": 2,
          "name": "business"
        },
        "emailAddress": "testgetbyemail@x.com",
        "tracking": {
          "createdOn": "2020-04-23",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-04-24 18:55:02",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-04-01T13:11:08",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-09-21T12:35:09",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 8,
    "firstName": "Sophia",
    "middleName": "Ophelia",
    "lastName": "Smith",
    "nickName": "sos",
    "dob": "1993-11-22T00:00:00",
    "addresses": [],
    "notes": [],
    "phones": [],
    "emails": [],
    "tracking": {
      "createdOn": "2020-04-27T15:49:47",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-04-27T15:49:47",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 9,
    "firstName": "Fake",
    "middleName": "Ophelia",
    "lastName": "Employee",
    "nickName": "sos",
    "dob": "1993-11-22T00:00:00",
    "addresses": [],
    "notes": [],
    "phones": [
      {
        "phoneNumberId": 1,
        "type": {
          "id": 1,
          "name": "home"
        },
        "countryCode": "123",
        "areaCode": "456",
        "subscriber": "7891011",
        "tracking": {
          "createdOn": "2020-04-01 13:02:41",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-05-20 14:50:24",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 2
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "patchingemail@patch.com",
        "tracking": {
          "createdOn": "2020-04-16",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-07-14 06:54:06",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 2
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-04-27T15:50:49",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-07-14T06:33:32",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 10,
    "firstName": "Znanatej",
    "middleName": "",
    "lastName": "P",
    "nickName": "Z",
    "dob": "2000-01-01T00:00:00",
    "addresses": [],
    "notes": [],
    "phones": [],
    "emails": [
      {
        "emailAddressId": 10,
        "type": {
          "id": 2,
          "name": "business"
        },
        "emailAddress": "test@test.com",
        "tracking": {
          "createdOn": "2020-08-12",
          "createdBy": {
            "name": "Znanatej P",
            "id": 729
          },
          "updatedOn": "2020-08-12 21:21:00",
          "updatedBy": {
            "name": "Znanatej P",
            "id": 729
          }
        }
      }
    ],
    "tracking": {
      "createdOn": null,
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": null,
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 11,
    "firstName": "Znanatej",
    "middleName": "",
    "lastName": "P",
    "nickName": "Z",
    "dob": "1980-01-01T00:00:00",
    "addresses": [],
    "notes": [],
    "phones": [],
    "emails": [
      {
        "emailAddressId": 11,
        "type": {
          "id": 2,
          "name": "business"
        },
        "emailAddress": "zpanga@ms3-inc.com",
        "tracking": {
          "createdOn": "2020-09-09",
          "createdBy": {
            "name": "Znanatej P",
            "id": 729
          },
          "updatedOn": "2020-09-09 01:10:00",
          "updatedBy": {
            "name": "Znanatej P",
            "id": 729
          }
        }
      }
    ],
    "tracking": {
      "createdOn": null,
      "createdBy": {
        "name": null,
        "id": null
      },
      "updatedOn": null,
      "updatedBy": {
        "name": null,
        "id": null
      }
    }
  },
  {
    "personId": 12,
    "firstName": "Sophia",
    "middleName": "Ophelia",
    "lastName": "Smith",
    "nickName": "sos",
    "dob": "1993-11-22T00:00:00",
    "addresses": [],
    "notes": [],
    "phones": [],
    "emails": [],
    "tracking": {
      "createdOn": "2020-04-27T17:34:50",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-04-27T17:34:50",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 13,
    "firstName": "Sophia",
    "middleName": "Ophelia",
    "lastName": "Smith",
    "nickName": "sos",
    "dob": "1988-08-28T00:00:00",
    "addresses": [],
    "notes": [],
    "phones": [],
    "emails": [],
    "tracking": {
      "createdOn": "2020-04-27T17:35:40",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-06-24T14:13:22",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 15,
    "firstName": "Sophia",
    "middleName": "Ophelia",
    "lastName": "Smith",
    "nickName": "sos",
    "dob": "1993-11-22T00:00:00",
    "addresses": [],
    "notes": [],
    "phones": [],
    "emails": [],
    "tracking": {
      "createdOn": "2020-04-27T17:43:30",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-04-27T17:43:30",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1243,
    "firstName": "Jacob",
    "middleName": "M",
    "lastName": "Hughes",
    "nickName": "Jake",
    "dob": "1995-12-31T00:00:00",
    "addresses": [
      {
        "addressId": 2022,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "MISSING???",
        "tracking": {
          "createdOn": "2020-08-24 20:10:01",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-24 20:10:01",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 642,
        "title": "flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-08-24 20:10:01",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-24 20:10:01",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "phones": [
      {
        "phoneNumberId": 1721,
        "type": {
          "id": 1,
          "name": "home"
        },
        "countryCode": "00",
        "areaCode": "607",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-08-24 20:10:01",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-24 20:10:01",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1836,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "ssmith@cfa.com",
        "tracking": {
          "createdOn": "2020-08-24",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-24 20:10:01",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-08-24T20:10:01",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-08-24T20:10:01",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1244,
    "firstName": "Test",
    "middleName": "null",
    "lastName": "McTesty",
    "nickName": "null",
    "dob": null,
    "addresses": [],
    "notes": [],
    "phones": [
      {
        "phoneNumberId": 1722,
        "type": {
          "id": 1,
          "name": "home"
        },
        "countryCode": "011",
        "areaCode": "607",
        "subscriber": "5551465",
        "tracking": {
          "createdOn": "2020-08-24 20:10:02",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-24 20:10:02",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [],
    "tracking": {
      "createdOn": "2020-08-24T20:10:02",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-08-24T20:10:02",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1245,
    "firstName": "Test",
    "middleName": "null",
    "lastName": "McTesty",
    "nickName": "null",
    "dob": null,
    "addresses": [],
    "notes": [],
    "phones": [
      {
        "phoneNumberId": 1723,
        "type": {
          "id": 1,
          "name": "home"
        },
        "countryCode": "011",
        "areaCode": "607",
        "subscriber": "5551465",
        "tracking": {
          "createdOn": "2020-08-24 20:10:02",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-24 20:10:02",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [],
    "tracking": {
      "createdOn": "2020-08-24T20:10:02",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-08-24T20:10:02",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1246,
    "firstName": "Test",
    "middleName": "Test",
    "lastName": "Test",
    "nickName": "sos",
    "dob": "1993-11-22T00:00:00",
    "addresses": [
      {
        "addressId": 2023,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "UTC-5",
        "tracking": {
          "createdOn": "2020-08-24 23:08:09",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-08-24 23:08:09",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 643,
        "title": "Flowers",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-08-24 23:08:09",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-08-24 23:08:09",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      }
    ],
    "phones": [
      {
        "phoneNumberId": 1724,
        "type": {
          "id": 2,
          "name": "work"
        },
        "countryCode": "011",
        "areaCode": "607",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-08-24 23:08:09",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-08-24 23:08:09",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1837,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "testtesttest@cfa.com",
        "tracking": {
          "createdOn": "2020-08-24",
          "createdBy": {
            "name": "Jane Doe",
            "id": 8
          },
          "updatedOn": "2020-08-24 23:08:09",
          "updatedBy": {
            "name": "Jane Doe",
            "id": 8
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-08-24T23:08:09",
      "createdBy": {
        "name": "Jane Doe",
        "id": 8
      },
      "updatedOn": "2020-08-24T23:08:09",
      "updatedBy": {
        "name": "Jane Doe",
        "id": 8
      }
    }
  },
  {
    "personId": 1247,
    "firstName": "Jake",
    "middleName": "Ophelia",
    "lastName": "Smith",
    "nickName": "sos",
    "dob": "1993-11-22T00:00:00",
    "addresses": [
      {
        "addressId": 2036,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "MISSING???",
        "tracking": {
          "createdOn": "2020-08-25 12:59:03",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-25 12:59:03",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 650,
        "title": "flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-08-25 12:59:03",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-25 12:59:03",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "phones": [
      {
        "phoneNumberId": 1731,
        "type": {
          "id": 1,
          "name": "home"
        },
        "countryCode": "00",
        "areaCode": "607",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-08-25 12:59:03",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-25 12:59:03",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1850,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "ssmith@cfa.com",
        "tracking": {
          "createdOn": "2020-08-25",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-25 12:59:03",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-08-25T12:59:03",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-08-25T12:59:07",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1248,
    "firstName": "Test",
    "middleName": "null",
    "lastName": "McTesty",
    "nickName": "null",
    "dob": null,
    "addresses": [],
    "notes": [],
    "phones": [
      {
        "phoneNumberId": 1732,
        "type": {
          "id": 1,
          "name": "home"
        },
        "countryCode": "011",
        "areaCode": "607",
        "subscriber": "5551465",
        "tracking": {
          "createdOn": "2020-08-25 12:59:03",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-25 12:59:03",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [],
    "tracking": {
      "createdOn": "2020-08-25T12:59:03",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-08-25T12:59:03",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1249,
    "firstName": "Test",
    "middleName": "null",
    "lastName": "McTesty",
    "nickName": "null",
    "dob": null,
    "addresses": [],
    "notes": [],
    "phones": [
      {
        "phoneNumberId": 1733,
        "type": {
          "id": 1,
          "name": "home"
        },
        "countryCode": "011",
        "areaCode": "607",
        "subscriber": "5551465",
        "tracking": {
          "createdOn": "2020-08-25 12:59:03",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-25 12:59:03",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [],
    "tracking": {
      "createdOn": "2020-08-25T12:59:03",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-08-25T12:59:03",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1250,
    "firstName": "Jacob",
    "middleName": "M",
    "lastName": "Hughes",
    "nickName": "Jake",
    "dob": "1995-12-31T00:00:00",
    "addresses": [
      {
        "addressId": 2037,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "MISSING???",
        "tracking": {
          "createdOn": "2020-08-25 14:04:17",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-25 14:04:17",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 651,
        "title": "flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-08-25 14:04:17",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-25 14:04:17",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "phones": [
      {
        "phoneNumberId": 1734,
        "type": {
          "id": 1,
          "name": "home"
        },
        "countryCode": "00",
        "areaCode": "607",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-08-25 14:04:17",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-25 14:04:17",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1851,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "ssmith@cfa.com",
        "tracking": {
          "createdOn": "2020-08-25",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-08-25 14:04:17",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-08-25T14:04:17",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-08-25T14:04:17",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1287,
    "firstName": "Sophia",
    "middleName": "Ophelia",
    "lastName": "Smith",
    "nickName": "null",
    "dob": null,
    "addresses": [
      {
        "addressId": 2087,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "null",
        "line2": "null",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "null",
        "tracking": {
          "createdOn": "2020-09-10 08:42:10",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:42:10",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 694,
        "title": "flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-09-10 08:42:10",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:42:10",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "phones": [
      {
        "phoneNumberId": 1778,
        "type": {
          "id": 2,
          "name": "work"
        },
        "countryCode": "00",
        "areaCode": "607",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-09-10 08:42:10",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:42:10",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1904,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "ssmith@cfa.com",
        "tracking": {
          "createdOn": "2020-09-10",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:42:10",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-09-10T08:42:10",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-09-10T08:42:10",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1288,
    "firstName": "Sophia",
    "middleName": "Ophelia",
    "lastName": "Smith",
    "nickName": "null",
    "dob": null,
    "addresses": [
      {
        "addressId": 2088,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "null",
        "line2": "null",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "null",
        "tracking": {
          "createdOn": "2020-09-10 08:44:37",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:44:37",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 695,
        "title": "flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-09-10 08:44:37",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:44:37",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "phones": [
      {
        "phoneNumberId": 1779,
        "type": {
          "id": 2,
          "name": "work"
        },
        "countryCode": "00",
        "areaCode": "607",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-09-10 08:44:37",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:44:37",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1905,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "ssmith@cfa.com",
        "tracking": {
          "createdOn": "2020-09-10",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:44:37",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-09-10T08:44:37",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-09-10T08:44:37",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1289,
    "firstName": "Sophia",
    "middleName": "Ophelia",
    "lastName": "Smith",
    "nickName": "null",
    "dob": null,
    "addresses": [
      {
        "addressId": 2089,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "null",
        "line2": "null",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "null",
        "tracking": {
          "createdOn": "2020-09-10 08:46:51",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:46:51",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 696,
        "title": "flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-09-10 08:46:51",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:46:51",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "phones": [
      {
        "phoneNumberId": 1780,
        "type": {
          "id": 2,
          "name": "work"
        },
        "countryCode": "00",
        "areaCode": "607",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-09-10 08:46:51",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:46:51",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1906,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "ssmith@cfa.com",
        "tracking": {
          "createdOn": "2020-09-10",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 08:46:51",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-09-10T08:46:51",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-09-10T08:46:51",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1290,
    "firstName": "Jacob",
    "middleName": "M",
    "lastName": "Hughes",
    "nickName": "Jake",
    "dob": "1995-12-31T00:00:00",
    "addresses": [
      {
        "addressId": 2090,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "1 Main Street",
        "line2": "Apt. 1",
        "city": "Charles Town",
        "region": "WV",
        "country": "US",
        "postalCode": "23232-4545",
        "timezone": "MISSING???",
        "tracking": {
          "createdOn": "2020-09-10 14:09:30",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 14:09:30",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 697,
        "title": "flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-09-10 14:09:30",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 14:09:30",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "phones": [
      {
        "phoneNumberId": 1781,
        "type": {
          "id": 1,
          "name": "home"
        },
        "countryCode": "00",
        "areaCode": "607",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-09-10 14:09:30",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 14:09:30",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1907,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "ssmith@cfa.com",
        "tracking": {
          "createdOn": "2020-09-10",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-10 14:09:30",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-09-10T14:09:30",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-09-10T14:09:30",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  },
  {
    "personId": 1308,
    "firstName": "Sophia",
    "middleName": "Ophelia",
    "lastName": "Smith",
    "nickName": "null",
    "dob": "2000-12-31T00:00:00",
    "addresses": [
      {
        "addressId": 2100,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "line1": "123",
        "line2": "Apt. 1",
        "city": "test",
        "region": "test",
        "country": "test",
        "postalCode": "23232-4545",
        "timezone": "null",
        "tracking": {
          "createdOn": "2020-09-14 08:54:41",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-21 13:17:05",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "notes": [
      {
        "noteId": 706,
        "title": "flower",
        "note": "Likes red roses.",
        "tracking": {
          "createdOn": "2020-09-14 08:54:41",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-21 13:17:05",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "phones": [
      {
        "phoneNumberId": 1801,
        "type": {
          "id": 2,
          "name": "work"
        },
        "countryCode": "00",
        "areaCode": "607",
        "subscriber": "5551212",
        "tracking": {
          "createdOn": "2020-09-14 08:54:41",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-21 13:17:05",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "emails": [
      {
        "emailAddressId": 1919,
        "type": {
          "id": 1,
          "name": "personal"
        },
        "emailAddress": "ssmith@cfa.com",
        "tracking": {
          "createdOn": "2020-09-14",
          "createdBy": {
            "name": "Jake Smith",
            "id": 1
          },
          "updatedOn": "2020-09-21 13:17:05",
          "updatedBy": {
            "name": "Jake Smith",
            "id": 1
          }
        }
      }
    ],
    "tracking": {
      "createdOn": "2020-09-14T08:54:41",
      "createdBy": {
        "name": "Jake Smith",
        "id": 1
      },
      "updatedOn": "2020-09-21T13:17:05",
      "updatedBy": {
        "name": "Jake Smith",
        "id": 1
      }
    }
  }]
var phones=[]


fun getPhoneNumbers(pepId)=
    if(pepId == null)
        []
    else
        (phones firstWith ((item) -> item.id == pepId)).phones default []

fun getPerson(pepId) = do {
var temp = persons filter(item,index) -> (item.personId == pepId)
---
    if(sizeOf(temp) > 0)
        temp[0]
    else
        {
            firstName: null,
            lastName: null,
            nickName: null,
            middleName: null
        }
}

var data=
    payload map ((item,index) -> item ++
        do {
            var person = getPerson(item.personId)
            ---
            {
                firstName: person.firstName,
                middleName: person.middleName,
                lastName: person.lastName,
                nickName: person.nickName,
                emergencyContacts:
                    item.emergencyContacts map ((contactId) -> do {
                        var contact = getPerson(contactId)
                        ---
                        {
                            personId: contactId,
                            firstName: contact.firstName,
                            lastName: contact.lastName,
                            phoneNumbers:
                                getPhoneNumbers(contactId) map ((phone) ->
                                phone - 'tracking')
                        }
                    })
            }
        }
    )

---

{
    "data": data,
    "cursor":{
        "nextCursor":
            if(sizeOf(data)>0)
                data[sizeOf(data)-1].employeeId + 1
            else null
    }
}

