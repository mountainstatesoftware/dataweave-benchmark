%dw 2.0
output application/json
var testPayload=[
  {
    "packageguid": "3F2B6DAC-648B-4313-AD4C-0479F8905B65",
    "finalprice": 17.5000,
    "corporatepricebookguid": "01179E4C-A254-E211-A0EA-001018288826",
    "serviceTypeNameId": 3,
    "priceByST": 12.2500
  },
  {
    "packageguid": "3F2B6DAC-648B-4313-AD4C-0479F8905B65",
    "finalprice": 17.5000,
    "corporatepricebookguid": "01179E4C-A254-E211-A0EA-001018288826",
    "serviceTypeNameId": 9,
    "priceByST": 5.2500
  },
  {
    "packageguid": "67AB87CC-0179-4EA8-8475-0D7C7AB5C4CF",
    "finalprice": 129.5000,
    "corporatepricebookguid": "01179E4C-A254-E211-A0EA-001018288826",
    "serviceTypeNameId": 3,
    "priceByST": 124.2500
  },
  {
    "packageguid": "67AB87CC-0179-4EA8-8475-0D7C7AB5C4CF",
    "finalprice": 129.5000,
    "corporatepricebookguid": "01179E4C-A254-E211-A0EA-001018288826",
    "serviceTypeNameId": 9,
    "priceByST": 5.2500
  },
  {
    "packageguid": "3F2B6DAC-648B-4313-AD4C-0479F8905B65",
    "finalprice": 0.0000,
    "corporatepricebookguid": "31922CD9-ACB7-4835-8220-FB268D8A781C",
    "serviceTypeNameId": 3,
    "priceByST": 0.0000
  },
  {
    "packageguid": "3F2B6DAC-648B-4313-AD4C-0479F8905B65",
    "finalprice": 0.0000,
    "corporatepricebookguid": "31922CD9-ACB7-4835-8220-FB268D8A781C",
    "serviceTypeNameId": 9,
    "priceByST": 0.0000
  },
  {
    "packageguid": "67AB87CC-0179-4EA8-8475-0D7C7AB5C4CF",
    "finalprice": 0.0000,
    "corporatepricebookguid": "31922CD9-ACB7-4835-8220-FB268D8A781C",
    "serviceTypeNameId": 3,
    "priceByST": 0.0000
  },
  {
    "packageguid": "67AB87CC-0179-4EA8-8475-0D7C7AB5C4CF",
    "finalprice": 0.0000,
    "corporatepricebookguid": "31922CD9-ACB7-4835-8220-FB268D8A781C",
    "serviceTypeNameId": 9,
    "priceByST": 0.0000
  }
]
---

(testPayload groupBy (item,index) -> item.corporatepricebookguid) pluck
    (pricebooks,k,i) -> {
        corporatePriceBookGuid: pricebooks[0].corporatepricebookguid,
        pricing: (
            (pricebooks groupBy (item,index) -> item.packageguid) pluck
                (packages,key,index) -> {
                    packageGuid: packages[0].packageguid,
                    price: packages[0].finalprice,
                    distribution: (
                        packages map
                            (package,index) -> {
                                serviceTypeNameId: package.serviceTypeNameId,
                                priceByST: package.priceByST
                            }

                    )
                }
        )
    }
