fun formatAsDate(dateStr) =  dateStr
fun buildPositionArr(id, name, start, end, current) = id default [] map ((item, index) -> {
            positionId: item,
			positionName: name[index],
			startDate: if(start[index] != 'NULL')
                            formatAsDate(start[index])
                        else null,
			endDate: if(end[index] != 'NULL')
                         formatAsDate(end[index])
                    else null,
			current: current[index]
})
fun buildEmergencyContactsArr(emp:Array<Object>) =  emp distinctBy $.id map ((item, index) -> item.emergency_contact_id)
---
{
    "employees": payload.resultSet1 map ((item,index) -> {
        employeeId: item.id,
        supervisor: if(item.supervisor == 1) true else false,
        supervisorId: item.supervisor_id,
        personId: item.person_id,
        recruiterId: item.recruiter_id,
        overallStartDate: formatAsDate(item.overall_start_date),
        overallEndDate: formatAsDate(item.overall_end_date),
        employmentType: {
            typeId: item.employment_type_id,
            name: item.employment_type_name
        },
        billable: item.billable,
        emergencyContacts:
            if(item.emergency_contact_id == null)
                []
            else
                item.emergency_contact_id splitBy "," map $ as Number,
        positions:
            if(item.position_ids != null)
                buildPositionArr(
                    item.position_ids splitBy "," map $ as Number,
                    item.position_names splitBy ",",
                    item.start_dates splitBy ",",
                    item.end_dates splitBy ",",
                    item.current splitBy "," map $ as Number
                )
            else [],
        active: item.active,
        tracking: {
            createdOn: formatAsDate( item.created_on),
            createdBy:{
                id:item.created_by,
                name: item.created_by_name
            },
            lastUpdated: formatAsDate( item.last_updated),
            updatedBy:{
                id:item.updated_by,
                name: item.updated_by_name
            }
        }
    })
}